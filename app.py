#	bin.py - pastebin.
#	Copyright (C) 2022 Ferass EL HAFIDI
#
#	This program is free software: you can redistribute it and/or modify
#	it under the terms of the GNU Affero General Public License as published by
#	the Free Software Foundation, either version 3 of the License, or
#	(at your option) any later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU Affero General Public License for more details.
#
#	You should have received a copy of the GNU Affero General Public License
#	along with this program.  If not, see <https://www.gnu.org/licenses/>.
from flask import Flask
from flask import render_template, request, redirect, abort
from flask.wrappers import Response
import time, os
app = Flask(__name__)

def allowed_file(filename):
	return '.' in filename and \
		filename.rsplit('.', 1)[1].lower() in { 'txt', 'md', 'log', 'c', 'cpp' }

@app.route('/', methods=['GET', 'POST'])
def index():
	if request.method == 'GET':
		return render_template('bin.html', bin_url = request.base_url)
	elif request.method == 'POST':
		paste = str(format(int(round(time.time(), 0)), 'x'))
		if "text" in request.form and \
				request.form["text"].replace(" ", "").replace("\n", "") != "":
			text = request.form["text"]
			file = open("pastes/%s.txt" % paste, mode='x')
			print("%s" % text, file=file)
			file.close()
		elif "file" in request.files and request.files["file"].filename != "":
			file_paste = request.files["file"]
			if not allowed_file(file_paste.filename):
				return Response("(!) File type isn't allowed. (!)\n", 
					mimetype='text/plain')
			if allowed_file(file_paste.filename):
				file_paste.save("pastes/%s.txt" % paste)
		else:
			return Response("(!) Paste is empty. (!)\n", mimetype='text/plain')
		return Response("New paste has been created: %s\n" % \
				paste, mimetype='text/plain')
	return "Invalid request\n", 400 # Never reached if the request is valid.

@app.route('/<paste>')
def get_paste(paste = None):
	try: file = open("pastes/%s.txt" % paste, mode='r')
	except FileNotFoundError:
		return Response("(!) Paste not found. (!)", mimetype='text/plain'), 404
	text = file.read()
	file.close()
	return Response("%s\n" % text, mimetype='text/plain')
