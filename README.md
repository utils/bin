# bin

*A simple pastebin.*

This is the pastebin software that powers bin.vitali64.duckdns.org.

```
What bin is:
 o Freedom-respecting!
 | ---------------------------------------------------------------------
 | Bin is software licensed under the GNU APLv3 license, which allows 
 | you to run it for any purpose, hack on, and redistribute! There's no 
 | advertising, tracking or anything like that: Just a pastebin.
 | 
 o Plain simple!
 | ---------------------------------------------------------------------
 | This pastebin software is *only* 45 SLOC long. This makes it really 
 | simple to understand and hack on! It can also accept requests when 
 | using curl.
 | 
 o Painless to setup!
   ---------------------------------------------------------------------
   Self-hosting bin is as easy as running a WSGI server and supplying 
   the app! Then, just stick in a `proxy_pass` in your nginx 
   configuration file and you're done!
```
